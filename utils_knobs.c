#include "utils_knobs.h"

// - function -----------------------------------------------------------------
uint8_t get_red_knob_value(uint8_t *mem_base)
{
    //NOT WORKING
    uint32_t value = *(volatile uint32_t *)(mem_base + SPILED_REG_KNOBS_8BIT_o);
    return (value >> 16) & 0xff;
}

// - function -----------------------------------------------------------------
uint8_t get_green_knob_value(uint8_t *mem_base)
{
    //NOT WORKING
    uint32_t value = *(volatile uint32_t *)(mem_base + SPILED_REG_KNOBS_8BIT_o);
    return (value >> 8) & 0xff;
}

// - function -----------------------------------------------------------------
uint8_t get_blue_knob_value(uint8_t *mem_base)
{
    //NOT WORKING
    uint32_t value = *(volatile uint32_t *)(mem_base + SPILED_REG_KNOBS_8BIT_o);
    return (value & 0xff);
}

// - function -----------------------------------------------------------------
uint32_t get_rgb_knobs_value(uint8_t *mem_base)
{
    //NOT WORKING
    uint32_t value = *(volatile uint32_t *)(mem_base + SPILED_REG_KNOBS_8BIT_o);
    return value;
}

// - function -----------------------------------------------------------------
bool is_pressed_red_knob(uint8_t *mem_base)
{
    uint32_t value = *(volatile uint32_t *)(mem_base + SPILED_REG_KNOBS_8BIT_o);
    if ((value >> 26) & 0x1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

// - function -----------------------------------------------------------------
bool is_pressed_green_knob(uint8_t *mem_base)
{
    uint32_t value = *(volatile uint32_t *)(mem_base + SPILED_REG_KNOBS_8BIT_o);
    if ((value >> 25) & 0x1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

// - function -----------------------------------------------------------------
bool is_pressed_blue_knob(uint8_t *mem_base)
{
    uint32_t value = *(volatile uint32_t *)(mem_base + SPILED_REG_KNOBS_8BIT_o);
    if ((value >> 24) & 0x1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

// - function -----------------------------------------------------------------
bool blue_knob_moved_right(uint8_t *mem_base, uint8_t *prev_val)
{
    uint8_t current_val = get_blue_knob_value(mem_base);
    bool ret_val = false;
    if ((*prev_val > current_val) && !(*prev_val < 15 && current_val > 240))
    {
        ret_val = true;
        *prev_val = current_val;
    }
    else if (*prev_val < 15 && current_val > 240)
    {
        ret_val = true;
        *prev_val = current_val;
    }

    return ret_val;
}

// - function -----------------------------------------------------------------
bool blue_knob_moved_left(uint8_t *mem_base, uint8_t *prev_val)
{
    uint8_t current_val = get_blue_knob_value(mem_base);
    bool ret_val = false;
    if ((*prev_val < current_val) && !(*prev_val < 15 && current_val > 240))
    {
        ret_val = true;
        *prev_val = current_val;
    }
    else if (*prev_val > 240 && current_val < 15)
    {
        ret_val = true;
        *prev_val = current_val;
    }

    return ret_val;
}

// - function -----------------------------------------------------------------
bool red_knob_moved_right(uint8_t *mem_base, uint8_t *prev_val)
{
    uint8_t current_val = get_red_knob_value(mem_base);
    bool ret_val = false;

    if ((*prev_val > current_val) && !(*prev_val < 15 && current_val > 240))
    {
        ret_val = true;
        *prev_val = current_val;
    }
    else if (*prev_val < 15 && current_val > 240)
    {
        ret_val = true;
        *prev_val = current_val;
    }

    return ret_val;
}

// - function -----------------------------------------------------------------
bool red_knob_moved_left(uint8_t *mem_base, uint8_t *prev_val)
{
    uint8_t current_val = get_red_knob_value(mem_base);
    bool ret_val = false;

    if ((*prev_val < current_val) && !(*prev_val < 15 && current_val > 240))
    {
        ret_val = true;
        *prev_val = current_val;
    }
    else if (*prev_val > 240 && current_val < 15)
    {
        ret_val = true;
        *prev_val = current_val;
    }

    return ret_val;
}
