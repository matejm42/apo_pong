#include "screen_menu.h"

// - function -----------------------------------------------------------------
int get_user_choice_menu(uint8_t *parlcd_mem_base, uint8_t *mem_base)
{
    uint16_t *lcd_buffer = allocate_memory(LCD_HEIGHT * LCD_WIDTH * sizeof(uint16_t));

    int ret_val = 0;
    bool run = true;
    while (run)
    {
        draw_menu_main(lcd_buffer, parlcd_mem_base);
        switch (get_input(mem_base))
        {
        case RED:
            ret_val = DEMO;
            run = false;
            break;
        case GREEN:
            ret_val = get_second_menu(&run, lcd_buffer, parlcd_mem_base, mem_base);
            break;
        case BLUE:
            ret_val = EXIT;
            run = false;
            break;
        default:
            break;
        }
    }
    my_mili_sleep(500);

    free(lcd_buffer);
    return ret_val;
}

// - function -----------------------------------------------------------------
int get_second_menu(bool *run, uint16_t *lcd_buffer, uint8_t *parlcd_mem_base, uint8_t *mem_base)
{
    int ret_val =0;
    draw_menu_secondary(lcd_buffer, parlcd_mem_base);
    my_mili_sleep(500);
    switch (get_input(mem_base))
    {
    case RED:
        *run = false;
        ret_val = PLAYER_VS_PC;
        break;
    case GREEN:
        *run = false;
        ret_val = PLAYER_VS_PLAYER;
        break;
    case BLUE:
        my_mili_sleep(500);
    default:
        break;
    }
    return ret_val;
}

// - function -----------------------------------------------------------------
void draw_menu_main(uint16_t *lcd_buffer, uint8_t *parlcd_mem_base)
{
    font_descriptor_t *font_desc = &font_winFreeSystem14x16;
    uint32_t font_colour = hsv_to_lcd_colours(0, 0, 255);
    int main_font_scale = 7;
    int second_font_scale = 4;

    clear_display(BACKGROUND_COLOR, lcd_buffer); //prepare display for menu

    draw_text("APO", 10, 10, font_colour, lcd_buffer, font_desc, main_font_scale, false);
    draw_text("PONG", 200, 10, font_colour, lcd_buffer, font_desc, main_font_scale, true);

    draw_text("Demo (R)", 130, 120, rgb_to_lcd_colours(255, 0, 0), lcd_buffer, font_desc, second_font_scale, false);
    draw_text("Play (G)", 150, 185, rgb_to_lcd_colours(0, 255, 0), lcd_buffer, font_desc, second_font_scale, false);
    draw_text("Exit (B)", 160, 250, rgb_to_lcd_colours(0, 0, 255), lcd_buffer, font_desc, second_font_scale, false);

    redraw_lcd_display(parlcd_mem_base, lcd_buffer);
}

// - function -----------------------------------------------------------------
void draw_menu_secondary(uint16_t *lcd_buffer, uint8_t *parlcd_mem_base)
{
    font_descriptor_t *font_desc = &font_winFreeSystem14x16;
    uint32_t font_colour = hsv_to_lcd_colours(0, 0, 255);
    int main_font_scale = 7;
    int second_font_scale = 3;

    clear_display(BACKGROUND_COLOR, lcd_buffer); //prepare display for menu

    draw_text("APO", 10, 10, font_colour, lcd_buffer, font_desc, main_font_scale, false);
    draw_text("PONG", 200, 10, font_colour, lcd_buffer, font_desc, main_font_scale, true);

    draw_text("Player vs PC (R)", 80, 140, rgb_to_lcd_colours(255, 0, 0), lcd_buffer, font_desc, second_font_scale, false);
    draw_text("Player vs Player (G)", 40, 200, rgb_to_lcd_colours(0, 255, 0), lcd_buffer, font_desc, second_font_scale, false);
    draw_text("Go back (B)", 120, 260, rgb_to_lcd_colours(0, 0, 255), lcd_buffer, font_desc, second_font_scale, false);

    redraw_lcd_display(parlcd_mem_base, lcd_buffer);
}

// - function -----------------------------------------------------------------
int get_input(uint8_t *mem_base)
{
    bool run = true;
    int return_number;
    call_stty(false);
    printf("Enter your choice: \n");
    while (run)
    {
        char c = getchar();
        if (c == 'r' || c == 'R' || is_pressed_red_knob(mem_base))
        {
            run = false;
            return_number = RED;
        }
        else if (c == 'g' || c == 'G' || is_pressed_green_knob(mem_base))
        {
            run = false;
            return_number = GREEN;
        }
        else if (c == 'b' || c == 'B' || is_pressed_blue_knob(mem_base))
        {
            run = false;
            return_number = BLUE;
        }
    }
    call_stty(true);
    return return_number;
}
