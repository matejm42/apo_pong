#include "utils_lcd.h"

// - function -----------------------------------------------------------------
void clear_display(uint16_t background_color, uint16_t *lcd_buffer)
{
    for (int i = 0; i < LCD_WIDTH * LCD_HEIGHT; i++)
    {
        lcd_buffer[i] = background_color;
    }
}

// - function -----------------------------------------------------------------
void draw_pixel(int x, int y, uint16_t colour, uint16_t *lcd_buffer)
{
    if (x < LCD_WIDTH && x >= 0 && y < LCD_HEIGHT && y >= 0)
    {
        lcd_buffer[y * LCD_WIDTH + x] = colour;
    }
}

// - function -----------------------------------------------------------------
void draw_scaled_pixel(int x, int y, uint16_t colour, uint16_t *lcd_buffer, int scale)
{
    for (int i = 0; i < scale; i++)
    {
        for (int j = 0; j < scale; j++)
        {
            draw_pixel(x + i, y + j, colour, lcd_buffer);
        }
    }
}

// - function -----------------------------------------------------------------
int get_char_width(font_descriptor_t *font_desc, char c)
{
    int width;

    if (!font_desc->width)
    {
        width = font_desc->maxwidth;
    }
    else
    {
        width = font_desc->width[c - font_desc->firstchar];
    }

    return width;
}

// - function -----------------------------------------------------------------
void draw_char(char char_to_draw, int x, int y, uint16_t colour, uint16_t *lcd_buffer, font_descriptor_t *font_desc, int scale, bool inverted)
{
    int char_width = get_char_width(font_desc, char_to_draw);
    const font_bits_t *ptr;
    if ((char_to_draw >= font_desc->firstchar) && (char_to_draw - font_desc->firstchar < font_desc->size))
    {
        if (font_desc->offset)
        {
            ptr = &font_desc->bits[font_desc->offset[char_to_draw - font_desc->firstchar]];
        }
        else
        {
            int bw = (font_desc->maxwidth + 15) / 16;
            ptr = &font_desc->bits[(char_to_draw - font_desc->firstchar) * bw * font_desc->height];
        }

        for (int i = 0; i < font_desc->height; i++)
        {
            font_bits_t line_value = *ptr;
            for (int j = 0; j < char_width; j++)
            {
                if (inverted)
                {
                    if (!(line_value & 0x8000))
                    {
                        draw_scaled_pixel(x + scale * j, y + scale * i, colour, lcd_buffer, scale);
                    }
                }
                else
                {
                    if (line_value & 0x8000)
                    {
                        draw_scaled_pixel(x + scale * j, y + scale * i, colour, lcd_buffer, scale);
                    }
                }

                line_value <<= 1;
            }
            ptr++;
        }
    }
}

// - function -----------------------------------------------------------------
void draw_text(char *text, int start_x, int start_y, uint16_t colour, uint16_t *lcd_buffer, font_descriptor_t *font_desc, int scale, bool inverted)
{
    for (int i = 0; i < strlen(text); i++)
    {
        draw_char(text[i], start_x, start_y, colour, lcd_buffer, font_desc, scale, inverted);
        start_x += get_char_width(font_desc, text[i]) * scale;
    }
}

// - function -----------------------------------------------------------------
uint8_t *get_parlcd_mem_base()
{
    uint8_t *parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    if (!parlcd_mem_base)
    {
        fprintf(stderr, "Getting of parlcd_mem_base failed.\n");
        exit(-1);
    }
    return parlcd_mem_base;
}

// - function -----------------------------------------------------------------
uint8_t *init_display()
{
    uint8_t *parlcd_mem_base = get_parlcd_mem_base();
    parlcd_hx8357_init(parlcd_mem_base);
    return parlcd_mem_base;
}

// - function -----------------------------------------------------------------
void redraw_lcd_display(uint8_t *parlcd_mem_base, uint16_t *lcd_buffer)
{
    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (int i = 0; i < LCD_WIDTH * LCD_HEIGHT; i++)
    {
        parlcd_write_data(parlcd_mem_base, lcd_buffer[i]);
    }
}

// - function -----------------------------------------------------------------
uint16_t hsv_to_lcd_colours(int hue, int saturation, int value)
{
    hue = (hue % 360);
    float f = ((hue % 60) / 60.0);
    int p = (value * (255 - saturation)) / 255;
    int q = (value * (255 - (saturation * f))) / 255;
    int t = (value * (255 - (saturation * (1.0 - f)))) / 255;
    uint32_t r, g, b;

    if (hue < 60)
    {
        r = value;
        g = t;
        b = p;
    }
    else if (hue < 120)
    {
        r = q;
        g = value;
        b = p;
    }
    else if (hue < 180)
    {
        r = p;
        g = value;
        b = t;
    }
    else if (hue < 240)
    {
        r = p;
        g = q;
        b = value;
    }
    else if (hue < 300)
    {
        r = t;
        g = p;
        b = value;
    }
    else
    {
        r = value;
        g = p;
        b = q;
    }

    r >>= 3;
    g >>= 2;
    b >>= 3;

    return (((r & 0x1f) << 11) | ((g & 0x3f) << 5) | (b & 0x1f));
}

// - function -----------------------------------------------------------------
uint16_t rgb_to_lcd_colours(int r, int g, int b)
{
    r >>= 3;
    g >>= 2;
    b >>= 3;

    return (((r & 0x1f) << 11) | ((g & 0x3f) << 5) | (b & 0x1f));
}
