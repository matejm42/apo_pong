#ifndef AUTONOMY_PLAYER_H
#define AUTONOMY_PLAYER_H

#include "game_colision_utils.h"

/*
* Function automatically updates paddles position acording to balls position
*/
void ai_update_paddle_position(paddle_t *paddle, ball_t *ball);

/*
* This function update right paddle position.
*/
void ai_update_right_paddle_position(paddle_t *paddle, ball_t *ball);

/*
* This function update left paddle position.
*/
void ai_update_left_paddle_position(paddle_t *paddle, ball_t *ball);

#endif
