/*******************************************************************
  Project main function template for MicroZed based MZ_APO board
  designed by Petr Porazil at PiKRON

  main.c      - main file

  Tomáš Mlynář, Miroslav Matějček

  This file is part of APO-pong.

    APO-pong is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    APO-pong is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with APO-pong.  If not, see <https://www.gnu.org/licenses/>.


 *******************************************************************/

#define _POSIX_C_SOURCE 200809L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>
#include <fcntl.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "screen_menu.h"
#include "player.h"
#include "autonomy_player.h"
#include "utils_lcd.h"
#include "utils_rgb_led.h"
#include "games.h"
#include "screen_goodbye.h"

/*
* This function reset all peripherals to the initial state.
*/
void reset_peripherals_end(uint8_t *parlcd_mem_base, uint8_t *mem_base);

// - main function ------------------------------------------------------------
int main(int argc, char *argv[])
{
  bool run = true;
  int ret_val = EXIT_SUCCESS;
  uint8_t *parlcd_mem_base = init_display();
  uint8_t *mem_base = get_mem_base();
  fcntl(STDIN_FILENO, F_SETFL, fcntl(STDIN_FILENO, F_GETFL, 0) | O_NONBLOCK); //non blocking input

  while (run)
  {
    int option = get_user_choice_menu(parlcd_mem_base, mem_base);
    switch (option)
    {
    case 1:
      start_demo(parlcd_mem_base, mem_base);
      break;
    case 2:
      start_player_vs_pc_game(parlcd_mem_base, mem_base);
      break;
    case 3:
      start_player_vs_player_game(parlcd_mem_base, mem_base);
      break;
    case 4:
      run = false;
      display_goodbye_screen(mem_base, parlcd_mem_base);
      reset_peripherals_end(parlcd_mem_base, mem_base);
      break;
    default:
      continue;
    }
  }
  return ret_val;
}

// - function -----------------------------------------------------------------
void reset_peripherals_end(uint8_t *parlcd_mem_base, uint8_t *mem_base)
{
  //reset display
  uint16_t *lcd_buffer = allocate_memory(LCD_HEIGHT * LCD_WIDTH * sizeof(uint16_t));
  clear_display(BACKGROUND_COLOR, lcd_buffer);
  redraw_lcd_display(parlcd_mem_base, lcd_buffer);
  free(lcd_buffer);

  //reset led line
  change_led_line(mem_base, 0);

  //reset RGB leds
  uint32_t colour = hsv_to_led_colours(0, 0, 0);
  change_rgb1(mem_base, colour);
  change_rgb2(mem_base, colour);
}
