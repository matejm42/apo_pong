#include "game_visual_utils.h"

// - function -----------------------------------------------------------------
void left_paddle_colision_visual(game_t *game)
{
    uint32_t red = rgb_to_led_colours(100, 0, 0);
    change_rgb1(game->mem_base, red);
    change_rgb2(game->mem_base, BLACK);
    safe_change_rgb_led_line(game->mem_base, game->number_of_bounces / 2 + 1);
}

// - function -----------------------------------------------------------------
void right_paddle_colision_visual(game_t *game)
{
    uint32_t blue = rgb_to_led_colours(0, 0, 100);
    change_rgb2(game->mem_base, blue);
    change_rgb1(game->mem_base, BLACK);
    safe_change_rgb_led_line(game->mem_base, game->number_of_bounces / 2 + 1);
}

// - function -----------------------------------------------------------------
void score_visual(game_t *game)
{
    uint32_t red = rgb_to_led_colours(100, 0, 0);
    uint32_t blue = rgb_to_led_colours(0, 0, 100);
    struct timespec delay = {.tv_sec = 0, .tv_nsec = 100000000};
    for (size_t i = 0; i < 5; i++)
    {
        change_rgb1(game->mem_base, red);
        change_rgb2(game->mem_base, blue);
        safe_change_rgb_led_line(game->mem_base, game->number_of_bounces / 2 + 1);
        clock_nanosleep(CLOCK_MONOTONIC, 0, &delay, NULL);

        change_rgb1(game->mem_base, BLACK);
        change_rgb2(game->mem_base, BLACK);
        change_led_line(game->mem_base, 0);
        clock_nanosleep(CLOCK_MONOTONIC, 0, &delay, NULL);
    }
}
