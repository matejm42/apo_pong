#ifndef UTILS_KNOBS_H
#define UTILS_KNOBS_H

#include <stdint.h>
#include <stdbool.h>

#include "mzapo_phys.h"
#include "mzapo_regs.h"

/*
* This function return value of red knob. 
*/
uint8_t get_red_knob_value(uint8_t *mem_base);

/*
* This function return value of green knob. 
*/
uint8_t get_green_knob_value(uint8_t *mem_base);

/*
* This function return value of blue knob. 
*/
uint8_t get_blue_knob_value(uint8_t *mem_base);

/*
* This function return rgb value of knobs.
*/
uint32_t get_rgb_knobs_value(uint8_t *mem_base);

/*
* This function return true red knob is pressed.
*/
bool is_pressed_red_knob(uint8_t *mem_base);

/*
* This function return true if green knob is pressed.
*/
bool is_pressed_green_knob(uint8_t *mem_base);

/*
* This function return true if blue knob is pressed.
*/
bool is_pressed_blue_knob(uint8_t *mem_base);

/*
* This function return true if blue knob moved to the right.
*/
bool blue_knob_moved_right(uint8_t *mem_base, uint8_t *prev_val);

/*
* This function return true if blue knob moved to the left.
*/
bool blue_knob_moved_left(uint8_t *mem_base, uint8_t *prev_val);

/*
* This function return true if red knob moved to the right.
*/
bool red_knob_moved_right(uint8_t *mem_base, uint8_t *prev_val);

/*
* This function return true if red knob moved to the left.
*/
bool red_knob_moved_left(uint8_t *mem_base, uint8_t *prev_val);

#endif
