#include "autonomy_player.h"

// - function -----------------------------------------------------------------
void ai_update_paddle_position(paddle_t *paddle, ball_t *ball)
{
    int paddle_center = paddle->upper_position + PADDLE_HEIGHT / 2;
    if (paddle_center < ball->position_y + BALL_SIZE / 2 - paddle->speed / 2)
    { // - paddle->speed so it stay calm when ball is near center
        paddle->upper_position += paddle->speed;
    }
    else if (paddle_center > ball->position_y + BALL_SIZE / 2 + paddle->speed / 2)
    {
        paddle->upper_position -= paddle->speed;
    }
    control_paddles_border_colision(paddle);
}

// - function -----------------------------------------------------------------
void ai_update_left_paddle_position(paddle_t *paddle, ball_t *ball)
{
    if (ball->speed_x <= 0)
    {
        ai_update_paddle_position(paddle, ball);
    }
}

// - function -----------------------------------------------------------------
void ai_update_right_paddle_position(paddle_t *paddle, ball_t *ball)
{
    if (ball->speed_x >= 0)
    {
        ai_update_paddle_position(paddle, ball);
    }
}
