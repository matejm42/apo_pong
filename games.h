#ifndef GAMES_H
#define GAMES_H

#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#include "game_utils.h"
#include "utils_lcd.h"
#include "autonomy_player.h"
#include "utils.h"
#include "player.h"
#include "utils_knobs.h"
#include "screen_win.h"
#include "screen_game.h"

#define DEMO_PLAYER_SPEED 5
#define PC_PLAYER_SPEED 5
#define HUMAN_PLAYER_SPEED 15

/*
* This function starts demo game (pc vs pc).
*/
void start_demo(uint8_t *parlcd_mem_base, uint8_t *mem_base);

/*
* This function starts player vs pc game.
*/
void start_player_vs_pc_game(uint8_t *parlcd_mem_base, uint8_t *mem_base);

/*
* This function starts player vs player game.
*/
void start_player_vs_player_game(uint8_t *parlcd_mem_base, uint8_t *mem_base);

#endif
