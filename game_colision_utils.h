#ifndef GAME_COLISION_UTILS
#define GAME_COLISION_UTILS

#include "game_visual_utils.h"

/*
* This function change balls position acording to his speed, checks borders
* and colosions with paddles. It returns int accoring to type of collision or 0 if collision did not happened.
*/
void update_balls_position(ball_t *ball);

/*
* If paddle is out of display, it changes it's upper_position and put it back
*/
void control_paddles_border_colision(paddle_t *paddle);

/*
* This function controls, that ball is in the display
* if it is not, it returns him and change its speed_y
*/
void control_balls_border_colision(ball_t *ball);

/*
* This function controls if ball is on left/right side of display
* It calls proper function from handle_left(right)_side(paddle)_colision
*/
void handle_colisions(ball_t *ball, paddle_t *left_paddle, paddle_t *right_paddle, game_t *game);

/*
* This function updates score and reset game state
* also calls score visual
*/
void handle_left_side_colision(ball_t *ball, paddle_t *left_paddle, paddle_t *right_paddle, game_t *game);

/*
* This function updates score and reset game state
* also calls score_visual
*/
void handle_right_side_colision(ball_t *ball, paddle_t *left_paddle, paddle_t *right_paddle, game_t *game);

/*
*  This function changes ball speed and calls left_paddle_colision_visual
* it also increase ball speed
*/
void handle_left_paddle_colision(ball_t *ball, paddle_t *left_paddle, game_t *game);

/*
*  This function changes ball speed and calls right_paddle_colision_visual
*/
void handle_right_paddle_colision(ball_t *ball, paddle_t *right_paddle, game_t *game);

#endif
