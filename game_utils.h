#ifndef GAME_UTILS_H
#define GAME_UTILS_H

#define _POSIX_C_SOURCE 200809L

#include <time.h>
#include <stdlib.h>

#include "utils_lcd.h"
#include "utils.h"
#include "utils_knobs.h"

#define PADDLE_OFSET 10
#define PADDLE_HEIGHT 75
#define PADDLE_WIDTH 10
#define BALL_SIZE 15
#define NOT_COLLISION 0
#define PADDLE_COLLISION 1
#define SIDE_COLLISION 2

typedef struct
{
    int upper_position;
    int speed;
} paddle_t;

typedef struct
{
    int position_x;
    int position_y;
    int speed_x;
    int speed_y;
} ball_t;

typedef struct
{
    int player1_score;
    int player2_score;
    int number_of_bounces;

    uint8_t *mem_base;
} game_t;

/*
* This function creates new game, initialises players scores and num of bounces to 0
* and returns game*.
*/
game_t *create_game(uint8_t *mem_base);

/*
* This function returns true, if one of players reaches score 10.
*/
bool is_game_over(game_t *game);

/*
* This function creates new ball, initialises start ball's speeds and coords
* and returns ball*.
*/
ball_t *create_ball();

/*
* This function creates new paddle, initialises paddle speed and start coords
* and eturns paddle*.
*/
paddle_t *create_paddle(int speed);

/*
* This function generate random number acording to position, where ball hitted the paddle
* If the paddle is hited in center, the number is small
*/
double compute_speed_y_change(ball_t *ball, paddle_t *paddle);

/*
* This function sets ball's position to the center and reset it's speed to default
*/
void reset_balls_position(ball_t *ball);

/*
* This function sets paddle's position to center
*/
void reset_paddles_position(paddle_t *paddle);

/*
* This function reset paddles' position, ball's position and bounce counter
* after one player scores
*/
void reset_game_position(ball_t *ball, paddle_t *left_paddle, paddle_t *right_paddle, game_t *game);

/*
* This function process console input.
*/
char get_game_input(uint8_t *mem_base, uint8_t *blue_knob_val, uint8_t *red_knob_val);

#endif
