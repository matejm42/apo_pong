#ifndef SCREEN_GAME_H
#define SCREEN_GAME_H

#include "game_utils.h"

/*
* This function display current game_screen.
*/
void display_game_screen(uint16_t *lcd_buffer, game_t *game, paddle_t *left_paddle, paddle_t *right_paddle, ball_t *ball, uint8_t *parlcd_mem_base, time_t start_time);

/*
* This functions draws paddles into lcd_buffer.
*/
void draw_paddles(paddle_t *left_paddle, paddle_t *right_paddle, uint32_t colour, uint16_t *lcd_buffer);

/*
* This functions draws ball into lcd_buffer.
*/
void draw_ball(ball_t *ball, uint32_t colour, uint16_t *lcd_buffer);

/*
* This function draws dashed line splitting screan to two parts
*/
void draw_middle_line(uint32_t colour, uint16_t *lcd_buffer);

/*
* This function draws line with given parameters.
*/
void draw_line(int x, int y, int width, int height, uint32_t colour, uint16_t *lcd_buffer);
/*
* This function draws booth scores into lcd _buffer
*/
void draw_players_score(game_t *game, uint32_t colour, uint16_t *lcd_buffer);

/*
* This function draws instruction, how to return to the menu for first seconds of the game
*/
void draw_return_instructions(time_t start_time, uint16_t *lcd_buffer);

#include "game_utils.h"
#endif
