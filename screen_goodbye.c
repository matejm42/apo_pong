#include "screen_goodbye.h"

// - function -----------------------------------------------------------------
void display_goodbye_screen(uint8_t *mem_base, uint8_t *parlcd_mem_base)
{
    uint16_t *lcd_buffer = allocate_memory(LCD_HEIGHT * LCD_WIDTH * sizeof(uint16_t));
    font_descriptor_t *font_desc = &font_winFreeSystem14x16;
    uint32_t font_colour = rgb_to_lcd_colours(255, 255, 255);

    clear_display(BACKGROUND_COLOR, lcd_buffer);

    draw_text("GOODBYE!", 20, LCD_HEIGHT / 4, font_colour, lcd_buffer, font_desc, 6, false);
    draw_text("Miroslav Matejcek | Tomas Mlynar", 15, 200, font_colour, lcd_buffer, font_desc, 2, false);
    draw_text("FEE CTU", 190, 250, font_colour, lcd_buffer, font_desc, 2, false);

    change_led_line(mem_base, 32);
    uint32_t yellow_colour = rgb_to_led_colours(255, 70, 0);
    change_rgb1(mem_base, yellow_colour);
    change_rgb2(mem_base, yellow_colour);

    redraw_lcd_display(parlcd_mem_base, lcd_buffer);
    sleep(5);
    free(lcd_buffer);
}
