#include "screen_game.h"

// - function -----------------------------------------------------------------
void display_game_screen(uint16_t *lcd_buffer, game_t *game, paddle_t *left_paddle, paddle_t *right_paddle, ball_t *ball, uint8_t *parlcd_mem_base, time_t start_time)
{
    uint32_t colour = rgb_to_lcd_colours(255, 255, 255);
    clear_display(BACKGROUND_COLOR, lcd_buffer);
    draw_return_instructions(start_time, lcd_buffer);
    draw_middle_line(colour, lcd_buffer);
    draw_players_score(game, colour, lcd_buffer);
    draw_paddles(left_paddle, right_paddle, colour, lcd_buffer);
    draw_ball(ball, colour, lcd_buffer);
    redraw_lcd_display(parlcd_mem_base, lcd_buffer);
}

// - function -----------------------------------------------------------------
void draw_paddles(paddle_t *left_paddle, paddle_t *right_paddle, uint32_t colour, uint16_t *lcd_buffer)
{
    //draw left paddle
    int x = PADDLE_OFSET;
    int y = left_paddle->upper_position;
    draw_line(x, y, PADDLE_WIDTH, PADDLE_HEIGHT, colour, lcd_buffer);

    //draw right paddle
    x = LCD_WIDTH - PADDLE_OFSET - PADDLE_WIDTH;
    y = right_paddle->upper_position;
    draw_line(x, y, PADDLE_WIDTH, PADDLE_HEIGHT, colour, lcd_buffer);
}

// - function -----------------------------------------------------------------
void draw_ball(ball_t *ball, uint32_t colour, uint16_t *lcd_buffer)
{
    draw_scaled_pixel(ball->position_x, ball->position_y, colour, lcd_buffer, BALL_SIZE);
}

// - function -----------------------------------------------------------------
void draw_middle_line(uint32_t colour, uint16_t *lcd_buffer)
{
    int line_width = 6;
    int line_height = 20;
    int space_height = 10;
    int x = LCD_WIDTH / 2 - line_width / 2;
    int y = 0;
    for (int i = 0; i < (LCD_HEIGHT / (line_height + space_height)) + 1; i++)
    {
        draw_line(x, y, line_width, line_height, colour, lcd_buffer);
        y += line_height + space_height;
    }
}

// - function -----------------------------------------------------------------
void draw_line(int x, int y, int width, int height, uint32_t colour, uint16_t *lcd_buffer)
{
    for (int i = x; i < x + width; i++)
    {
        for (int j = y; j < y + height; j++)
        {
            draw_pixel(i, j, colour, lcd_buffer);
        }
    }
}

// - function -----------------------------------------------------------------
void draw_players_score(game_t *game, uint32_t colour, uint16_t *lcd_buffer)
{
    font_descriptor_t *font = &font_winFreeSystem14x16;
    int scale = 4;
    int x = LCD_WIDTH / 5;
    int y = 5;
    char score[3]; // because 10 are 2 chars, so we can't use draw_char
    sprintf(score, "%d", game->player1_score);
    draw_text(score, x, y, colour, lcd_buffer, font, scale, false);
    x = LCD_WIDTH * 4 / 5 - get_char_width(font, game->player2_score + '0') * scale;
    sprintf(score, "%d", game->player2_score);
    draw_text(score, x, y, colour, lcd_buffer, font, scale, false);
}

// - function -----------------------------------------------------------------
void draw_return_instructions(time_t start_time, uint16_t *lcd_buffer)
{
    time_t act_time = time(NULL);
    if (act_time - start_time < 3)
    {
        font_descriptor_t *font_desc = &font_winFreeSystem14x16;
        draw_text("Press green knob to exit (e)", 60, LCD_HEIGHT / 2, rgb_to_lcd_colours(0, 255, 0), lcd_buffer, font_desc, 2, false);
    }
}
