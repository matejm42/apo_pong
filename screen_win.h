#ifndef SCREEN_WIN_H
#define SCREEN_WIN_H

#include <stdint.h>
#include <unistd.h>

#include "utils_lcd.h"
#include "game_utils.h"
#include "utils_rgb_led.h"

/*
* This function display winner on lcd display.
*/
void display_winner(game_t *game, uint16_t *lcd_buffer, uint8_t *get_parlcd_mem_base);

#endif
