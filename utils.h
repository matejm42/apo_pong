#ifndef UTILS_H
#define UTILS_H

#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

/*
* This function allocates memory using malloc and returns pointer on it.
*/
void *allocate_memory(size_t size);
/*
 * This function returns absolute value of number
 */
int my_abs(int number);

/*
* This function set terminal to raw mode.
*/
void call_stty(bool reset);

/*
* This function spleeps for time in milisec
*/
void my_mili_sleep(unsigned int milisec);

#endif
