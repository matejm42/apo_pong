#ifndef UTILS_LCD_H
#define UTILS_LCD_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "font_types.h"
#include "mzapo_parlcd.h"
#include "mzapo_regs.h"
#include "mzapo_phys.h"

#define LCD_WIDTH 480
#define LCD_HEIGHT 320
#define BLACK 0
#define BACKGROUND_COLOR 0

/*
* This function sets all pixels in lcd_buffer to background collor
*/
void clear_display(uint16_t background_color, uint16_t *lcd_buffer);

/*
* This function draws one pixel on position defined by the x and y position to
* the lcd buffer.
*/
void draw_pixel(int x, int y, uint16_t colour, uint16_t *lcd_buffer);

/*
* This function calls draw pixel for many times to draw scaled pixel.
*/
void draw_scaled_pixel(int x, int y, uint16_t colour, uint16_t *lcd_buffer, int scale);

/*
* This function return width of entered char in entered font.
*/
int get_char_width(font_descriptor_t *font_desc, char c);

/*
* This function draw one char of entered font_descriptor to lcd_buffer.
*/
void draw_char(char char_to_draw, int x, int y, uint16_t colour, uint16_t *lcd_buffer, font_descriptor_t *font_desc, int scale, bool inverted);

/*
 * This function draw text to lcd_buffer.
 */
void draw_text(char *text, int start_x, int start_y, uint16_t colour, uint16_t *lcd_buffer, font_descriptor_t *font_desc, int scale, bool inverted);

/*
* This function return parlcd_membase.
*/
uint8_t *get_parlcd_mem_base();

/*
* This function initialize display and return parlcd_membase.
*/
uint8_t *init_display();

/*
* This function redraw whole display with given lcd_buffer.
*/
void redraw_lcd_display(uint8_t *parlcd_mem_base, uint16_t *lcd_buffer);

/*
* This function return color value needed for LCD display.
*/
uint16_t hsv_to_lcd_colours(int hue, int saturation, int value);

/*
* This function return colour value needed for LCD display from rgb.
*/
uint16_t rgb_to_lcd_colours(int r, int g, int b);

#endif
