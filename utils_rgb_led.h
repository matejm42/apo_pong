#ifndef UTILS_RGB_LED_H
#define UTILS_RGB_LED_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "mzapo_regs.h"
#include "mzapo_phys.h"

/*
* This function return rgb mem base value for accesing rgb leds, led line
* and knobs
*/
uint8_t *get_mem_base();

/*
* This function change colour of RGB1
*/
void change_rgb1(uint8_t *mem_base, uint32_t colour);

/*
* This function change colour of RGB2
*/
void change_rgb2(uint8_t *mem_base, uint32_t colour);

/*
* This function change number of shining LED diodes.
*/
void change_led_line(uint8_t *mem_base, int num_of_on);

/*
 * This function calls change_led_line, if num_of_on isn't from 
 * <0;32> it calls it with 0 or 32.
 */
void safe_change_rgb_led_line(uint8_t *mem_base, int num_of_on);

/*
* This function return colour for RGB leds based on hsv value.
*/
uint32_t hsv_to_led_colours(int hue, int saturation, int value);

/*
* This function return color for RGB leds based on rgb value.
*/
uint32_t rgb_to_led_colours(int r, int g, int b);

#endif
