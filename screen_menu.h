#ifndef SCREEN_MENU_H
#define SCREEN_MENU_H

#include <unistd.h>

#include "utils_lcd.h"
#include "utils.h"
#include "utils_knobs.h"

#define RED 1
#define GREEN 2
#define BLUE 3
#define DEMO 1
#define PLAYER_VS_PC 2
#define PLAYER_VS_PLAYER 3
#define EXIT 4

/*
* This function oversees menu creating and return number of user choice from menu.
*/
int get_user_choice_menu();

/*
* This function oversees second_menu creating, when user choose play option
* and return number of user choice from menu.
*/
int get_second_menu(bool *run, uint16_t *lcd_buffer, uint8_t *parlcd_mem_base, uint8_t *mem_base);

/*
* This function draw menu into lcd_buffer.
*/
void draw_menu_main(uint16_t *lcd_buffer, uint8_t *parlcd_mem_base);

/*
* This function draw secondary menu into lcd_buffer.
*/
void draw_menu_secondary(uint16_t *lcd_buffer, uint8_t *parlcd_mem_base);

/*
* This function return number of choice.
*/
int get_input();

#endif
