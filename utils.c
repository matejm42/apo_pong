#include "utils.h"

// - function -----------------------------------------------------------------
void *allocate_memory(size_t size)
{
    void *allocated_memory = malloc(size);
    if (!allocated_memory)
    {
        fprintf(stderr, "Allocation of memory failed!\n");
        exit(-1);
    }
    return allocated_memory;
}

// - function -----------------------------------------------------------------
int my_abs(int number)
{
    return number >= 0 ? number : -number;
}

// - function -----------------------------------------------------------------
void call_stty(bool reset)
{
    if (reset)
    {
        if (system("stty -raw opost echo"))
        {
            fprintf(stderr, "Failed to set terminal back from RAW mode!\n");
        }
    }
    else
    {
        if (system("stty raw opost -echo"))
        {
            fprintf(stderr, "Failed to set terminal into RAW mode!\n");
        }
    }
}

// - function -----------------------------------------------------------------
void my_mili_sleep(unsigned int milisec)
{
    unsigned int sec = milisec > 1000 ? milisec / 1000 : 0;
    milisec = milisec - 1000 * sec;
    unsigned int nanosec = 1000000 * milisec;
    struct timespec delay = {.tv_sec = sec, .tv_nsec = nanosec};
    clock_nanosleep(CLOCK_MONOTONIC, 0, &delay, NULL);
}
