#include "screen_win.h"

// - function -----------------------------------------------------------------
void display_winner(game_t *game, uint16_t *lcd_buffer, uint8_t *parlcd_mem_base)
{
    font_descriptor_t *font_desc = &font_winFreeSystem14x16;
    uint32_t font_colour = rgb_to_led_colours(0, 0, 0);
    uint32_t background_colour = rgb_to_lcd_colours(255, 255, 255);

    clear_display(background_colour, lcd_buffer);

    if (game->player1_score >= 10)
    {
        draw_text("PLAYER 1", 10, 50, font_colour, lcd_buffer, font_desc, 7, false);
    }
    else
    {
        draw_text("PLAYER 2", 10, 50, font_colour, lcd_buffer, font_desc, 7, false);
    }

    draw_text("WIN!", 90, 140, font_colour, lcd_buffer, font_desc, 10, false);

    redraw_lcd_display(parlcd_mem_base, lcd_buffer);

    sleep(5);
}
