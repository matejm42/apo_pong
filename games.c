#include "games.h"

// - function -----------------------------------------------------------------
void start_demo(uint8_t *parlcd_mem_base, uint8_t *mem_base)
{
    // Init
    printf("Demo started\n");
    printf("Enter e to exit:\n");
    game_t *game = create_game(mem_base);
    ball_t *ball = create_ball();
    paddle_t *left_paddle = create_paddle(DEMO_PLAYER_SPEED);
    paddle_t *right_paddle = create_paddle(DEMO_PLAYER_SPEED);
    uint16_t *lcd_buffer = allocate_memory(LCD_HEIGHT * LCD_WIDTH * sizeof(uint16_t));

    call_stty(false);

    time_t start_time = time(NULL);
    bool run = true;
    bool game_over;
    while (!(game_over = is_game_over(game)) && run)
    {
        char c;
        c = getchar();
        if (c == 'e' || c == 'E' || is_pressed_green_knob(mem_base))
        {
            run = false;
        }

        update_balls_position(ball);
        ai_update_left_paddle_position(left_paddle, ball);
        ai_update_right_paddle_position(right_paddle, ball);
        handle_colisions(ball, left_paddle, right_paddle, game);

        display_game_screen(lcd_buffer, game, left_paddle, right_paddle, ball, parlcd_mem_base, start_time);
    }
    call_stty(true);
    my_mili_sleep(300);

    if (game_over)
    {
        display_winner(game, lcd_buffer, parlcd_mem_base);
    }

    free(ball);
    free(left_paddle);
    free(right_paddle);
    free(game);
    free(lcd_buffer);
}

// - function -----------------------------------------------------------------
void start_player_vs_pc_game(uint8_t *parlcd_mem_base, uint8_t *mem_base)
{
    // Init
    printf("Demo started\n");
    printf("Enter e to exit:\n");
    game_t *game = create_game(mem_base);
    ball_t *ball = create_ball();
    paddle_t *left_paddle = create_paddle(HUMAN_PLAYER_SPEED);
    paddle_t *right_paddle = create_paddle(PC_PLAYER_SPEED);
    uint16_t *lcd_buffer = allocate_memory(LCD_HEIGHT * LCD_WIDTH * sizeof(uint16_t));

    call_stty(false);

    time_t start_time = time(NULL);
    bool run = true;
    bool game_over;
    uint8_t blue_knob_val = get_blue_knob_value(mem_base);
    uint8_t red_knob_val = get_red_knob_value(mem_base);
    while (!(game_over = is_game_over(game)) && run)
    {
        switch (get_game_input(mem_base, &blue_knob_val, &red_knob_val))
        {
        case 'e':
            run = false;
            break;
        case 'w':
        case 'i':
            move_paddle_up(left_paddle);
            break;
        case 's':
        case 'k':
            move_paddle_down(left_paddle);
            break;
        default:
            break;
        }

        ai_update_paddle_position(right_paddle, ball);

        update_balls_position(ball);
        handle_colisions(ball, left_paddle, right_paddle, game);

        display_game_screen(lcd_buffer, game, left_paddle, right_paddle, ball, parlcd_mem_base, start_time);
    }
    call_stty(true);
    my_mili_sleep(300);

    if (game_over)
    {
        display_winner(game, lcd_buffer, parlcd_mem_base);
    }

    free(ball);
    free(left_paddle);
    free(right_paddle);
    free(game);
    free(lcd_buffer);
}

// - function -----------------------------------------------------------------
void start_player_vs_player_game(uint8_t *parlcd_mem_base, uint8_t *mem_base)
{
    // Init
    printf("Player vs player game has started!\n");
    printf("Enter e to exit:\n");

    game_t *game = create_game(mem_base);
    ball_t *ball = create_ball();
    paddle_t *left_paddle = create_paddle(HUMAN_PLAYER_SPEED);
    paddle_t *right_paddle = create_paddle(HUMAN_PLAYER_SPEED);
    uint16_t *lcd_buffer = allocate_memory(LCD_HEIGHT * LCD_WIDTH * sizeof(uint16_t));

    call_stty(false);

    time_t start_time = time(NULL);
    bool run = true;
    bool game_over;
    uint8_t blue_knob_val = get_blue_knob_value(mem_base);
    uint8_t red_knob_val = get_red_knob_value(mem_base);
    while (!(game_over = is_game_over(game)) && run)
    {
        switch (get_game_input(mem_base, &blue_knob_val, &red_knob_val))
        {
        case 'e':
            run = false;
            break;
        case 'w':
            move_paddle_up(left_paddle);
            break;
        case 's':
            move_paddle_down(left_paddle);
            break;
        case 'i':
            move_paddle_up(right_paddle);
            break;
        case 'k':
            move_paddle_down(right_paddle);
            break;
        default:
            break;
        }

        update_balls_position(ball);
        handle_colisions(ball, left_paddle, right_paddle, game);

        display_game_screen(lcd_buffer, game, left_paddle, right_paddle, ball, parlcd_mem_base, start_time);
    }
    call_stty(true);
    my_mili_sleep(300);

    if (game_over)
    {
        display_winner(game, lcd_buffer, parlcd_mem_base);
    }

    free(ball);
    free(left_paddle);
    free(right_paddle);
    free(game);
    free(lcd_buffer);
}
