#include "game_colision_utils.h"

// - function -----------------------------------------------------------------
void update_balls_position(ball_t *ball)
{
    ball->position_x += ball->speed_x;
    ball->position_y += ball->speed_y;

    control_balls_border_colision(ball);
}

// - function -----------------------------------------------------------------
void handle_colisions(ball_t *ball, paddle_t *left_paddle, paddle_t *right_paddle, game_t *game)
{
    if (ball->position_x <= 0)
    {
        handle_left_side_colision(ball, left_paddle, right_paddle, game);
    }
    else if (ball->position_x <= PADDLE_OFSET + PADDLE_WIDTH)
    {
        if (ball->position_y + BALL_SIZE >= left_paddle->upper_position && ball->position_y <= left_paddle->upper_position + PADDLE_HEIGHT)
        {
            handle_left_paddle_colision(ball, left_paddle, game);
        }
    }

    if (ball->position_x + BALL_SIZE >= LCD_WIDTH)
    {
        handle_right_side_colision(ball, left_paddle, right_paddle, game);
    }
    else if (ball->position_x + BALL_SIZE >= LCD_WIDTH - PADDLE_OFSET - PADDLE_WIDTH)
    {
        if (ball->position_y + BALL_SIZE >= right_paddle->upper_position && ball->position_y <= right_paddle->upper_position + PADDLE_HEIGHT)
        {
            handle_right_paddle_colision(ball, right_paddle, game);
        }
    }
}

// - function -----------------------------------------------------------------
void handle_left_side_colision(ball_t *ball, paddle_t *left_paddle, paddle_t *right_paddle, game_t *game)
{
    score_visual(game);

    game->player2_score += 1;
    reset_game_position(ball, left_paddle, right_paddle, game);
}

// - function -----------------------------------------------------------------
void handle_right_side_colision(ball_t *ball, paddle_t *left_paddle, paddle_t *right_paddle, game_t *game)
{
    score_visual(game);

    game->player1_score += 1;
    reset_game_position(ball, left_paddle, right_paddle, game);
}

// - function -----------------------------------------------------------------
void handle_left_paddle_colision(ball_t *ball, paddle_t *left_paddle, game_t *game)
{
    ball->position_x = PADDLE_OFSET + PADDLE_WIDTH + 1;
    ball->speed_x = my_abs(ball->speed_x);
    game->number_of_bounces += 1;
    ball->speed_x += 1;
    ball->speed_y += compute_speed_y_change(ball, left_paddle);

    left_paddle_colision_visual(game);
}

// - function -----------------------------------------------------------------
void handle_right_paddle_colision(ball_t *ball, paddle_t *right_paddle, game_t *game)
{
    ball->position_x = LCD_WIDTH - PADDLE_OFSET - PADDLE_WIDTH - BALL_SIZE - 1;
    ball->speed_x = -my_abs(ball->speed_x);
    game->number_of_bounces += 1;
    ball->speed_y += compute_speed_y_change(ball, right_paddle);

    right_paddle_colision_visual(game);
}
// - function -----------------------------------------------------------------
void control_balls_border_colision(ball_t *ball)
{
    if (ball->position_y + BALL_SIZE > LCD_HEIGHT)
    {
        ball->position_y = LCD_HEIGHT - BALL_SIZE;
        ball->speed_y = -my_abs(ball->speed_y);
    }
    if (ball->position_y <= 0)
    {
        ball->position_y = 0;
        ball->speed_y = my_abs(ball->speed_y);
    }
}

// - function -----------------------------------------------------------------
void control_paddles_border_colision(paddle_t *paddle)
{
    if (paddle->upper_position < 0)
    {
        paddle->upper_position = 0;
    }
    if (paddle->upper_position + PADDLE_HEIGHT > LCD_HEIGHT)
    {
        paddle->upper_position = LCD_HEIGHT - PADDLE_HEIGHT;
    }
}
