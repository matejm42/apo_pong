#ifndef GAME_VISUAL_UTILS
#define GAME_VISUAL_UTILS

#define _POSIX_C_SOURCE 200809L

#include "utils_rgb_led.h"
#include "game_utils.h"

/*
* This function turn of the right LED and turns on the left one
*/
void left_paddle_colision_visual(game_t *game);

/*
* This function turn of the left LED and turns on the right one
*/
void right_paddle_colision_visual(game_t *game);

/*
* This function makes LEDs bling
*/
void score_visual(game_t *game);

#endif
