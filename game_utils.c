#include "game_utils.h"

// - function -----------------------------------------------------------------
game_t *create_game(uint8_t *mem_base)
{
    game_t *game = (game_t *)allocate_memory(sizeof(game_t));
    game->player1_score = 0;
    game->player2_score = 0;
    game->number_of_bounces = 0;

    game->mem_base = mem_base;
    return game;
}

// - function -----------------------------------------------------------------
ball_t *create_ball()
{
    ball_t *ball = (ball_t *)allocate_memory(sizeof(ball_t));
    reset_balls_position(ball);
    return ball;
}

// - function -----------------------------------------------------------------
paddle_t *create_paddle(int speed)
{
    paddle_t *paddle = (paddle_t *)allocate_memory(sizeof(paddle_t));
    paddle->speed = speed;
    reset_paddles_position(paddle);
    return paddle;
}

// - function -----------------------------------------------------------------
bool is_game_over(game_t *game)
{
    return game->player1_score >= 10 || game->player2_score >= 10;
}

// - function -----------------------------------------------------------------
double compute_speed_y_change(ball_t *ball, paddle_t *paddle)
{
    // set speed_y_change from -PADDLE_HEIGT/2 to PADDLE_HEIGT/2 depends where ball hits the paddle
    double speed_y_change = (ball->position_y + BALL_SIZE / 2) - (paddle->upper_position + PADDLE_HEIGHT / 2);
    speed_y_change /= ((PADDLE_HEIGHT / 2) / 5);              //set speed_y_change from -5 to 5
    speed_y_change *= ((double)(rand() % ball->speed_x)) / 3; //and we randomly multiply it by (0, speed_x/3)
    if (speed_y_change < 1 && speed_y_change > -1)
    {
        speed_y_change = 1; //So there is not perfect position for paddles
    }
    return speed_y_change;
}

// - function -----------------------------------------------------------------
void reset_balls_position(ball_t *ball)
{
    ball->position_x = LCD_WIDTH / 2 - BALL_SIZE / 2;
    ball->position_y = LCD_HEIGHT / 2 - BALL_SIZE / 2;

    //make random 1/-1 to choose who starts
    int random = rand() % 2;
    random = random ? 1 : -1;

    ball->speed_x = 5 * random; //its 5 or -5
    ball->speed_y = rand() % 10 - 5;
}

// - function -----------------------------------------------------------------
void reset_game_position(ball_t *ball, paddle_t *left_paddle, paddle_t *right_paddle, game_t *game)
{
    reset_balls_position(ball);
    reset_paddles_position(left_paddle);
    reset_paddles_position(right_paddle);
    game->number_of_bounces = 0;
}

// - function -----------------------------------------------------------------
void reset_paddles_position(paddle_t *paddle)
{
    paddle->upper_position = LCD_HEIGHT / 2 - PADDLE_HEIGHT / 2;
}

// - function -----------------------------------------------------------------
char get_game_input(uint8_t *mem_base, uint8_t *blue_knob_val, uint8_t *red_knob_val)
{
    char c = getchar();
    char ret_char = '0';

    if (c == 'e' || c == 'E' || is_pressed_green_knob(mem_base))
    {
        ret_char = 'e';
    }
    else if (c == 'w' || c == 'W' || red_knob_moved_left(mem_base, red_knob_val))
    {
        ret_char = 'w';
    }
    else if (c == 's' || c == 'S' || red_knob_moved_right(mem_base, red_knob_val))
    {
        ret_char = 's';
    }
    else if (c == 'i' || c == 'I' || blue_knob_moved_right(mem_base, blue_knob_val))
    {
        ret_char = 'i';
    }
    else if (c == 'k' || c == 'K' || blue_knob_moved_left(mem_base, blue_knob_val))
    {
        ret_char = 'k';
    }

    return ret_char;
}
