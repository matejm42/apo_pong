#ifndef PLAYER_H
#define PLAYER_H

#include "game_colision_utils.h"

/*
* This function move paddle up.
*/
void move_paddle_up(paddle_t *paddle);

/*
* This function move paddle down.
*/
void move_paddle_down(paddle_t *paddle);

#endif
