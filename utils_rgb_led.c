#include "utils_rgb_led.h"

// - function -----------------------------------------------------------------
uint8_t *get_mem_base()
{
    uint8_t *mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if (!mem_base)
    {
        fprintf(stderr, "Getting of mem_base failed.\n");
        exit(-1);
    }
    return mem_base;
}

// - function -----------------------------------------------------------------
void change_rgb1(uint8_t *mem_base, uint32_t colour)
{
    *(volatile uint32_t *)(mem_base + SPILED_REG_LED_RGB1_o) = colour;
}

// - function -----------------------------------------------------------------
void change_rgb2(uint8_t *mem_base, uint32_t colour)
{
    *(volatile uint32_t *)(mem_base + SPILED_REG_LED_RGB2_o) = colour;
}

// - function -----------------------------------------------------------------
void change_led_line(uint8_t *mem_base, int num_of_on)
{
    uint32_t value = 0;
    if (num_of_on >= 0 && num_of_on <= 32)
    {
        for (int i = 0; i < num_of_on; i++)
        {
            if (i == 0)
            {
                value += 1;
            }
            else
            {
                int power = 2;
                for (int j = 1; j < i; j++)
                {
                    power *= 2;
                }
                value += power;
            }
        }
    }
    else
    {
        fprintf(stderr, "Not valid value for led line!\n");
    }

    *(volatile uint32_t *)(mem_base + SPILED_REG_LED_LINE_o) = value;
}

// - function -----------------------------------------------------------------
void safe_change_rgb_led_line(uint8_t *mem_base, int num_of_on)
{
    if (num_of_on < 0)
    {
        change_led_line(mem_base, 0);
    }
    else if (num_of_on > 32)
    {
        change_led_line(mem_base, 32);
    }
    else
    {
        change_led_line(mem_base, num_of_on);
    }
}

// - function -----------------------------------------------------------------
uint32_t hsv_to_led_colours(int hue, int saturation, int value)
{
    hue = (hue % 360);
    float f = ((hue % 60) / 60.0);
    int p = (value * (255 - saturation)) / 255;
    int q = (value * (255 - (saturation * f))) / 255;
    int t = (value * (255 - (saturation * (1.0 - f)))) / 255;
    uint32_t r, g, b;

    if (hue < 60)
    {
        r = value;
        g = t;
        b = p;
    }
    else if (hue < 120)
    {
        r = q;
        g = value;
        b = p;
    }
    else if (hue < 180)
    {
        r = p;
        g = value;
        b = t;
    }
    else if (hue < 240)
    {
        r = p;
        g = q;
        b = value;
    }
    else if (hue < 300)
    {
        r = t;
        g = p;
        b = value;
    }
    else
    {
        r = value;
        g = p;
        b = q;
    }

    return ((r << 16) | (g << 8) | b);
}

// - function -----------------------------------------------------------------
uint32_t rgb_to_led_colours(int r, int g, int b)
{
    return ((r << 16) | (g << 8) | b);
}
