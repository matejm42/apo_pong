#ifndef SCREEN_GOODBYE_H
#define SCREEN_GOODBYE_H

#include <stdint.h>
#include <unistd.h>

#include "utils_lcd.h"
#include "utils.h"
#include "utils_rgb_led.h"

/*
* This function display goodbye screen.
*/
void display_goodbye_screen(uint8_t *mem_base, uint8_t *parlcd_mem_base);

#endif
