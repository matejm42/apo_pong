#include "player.h"

// - function -----------------------------------------------------------------
void move_paddle_up(paddle_t *paddle)
{
    paddle->upper_position -= paddle->speed;

    control_paddles_border_colision(paddle);
}

// - function -----------------------------------------------------------------
void move_paddle_down(paddle_t *paddle)
{
    paddle->upper_position += paddle->speed;

    control_paddles_border_colision(paddle);
}
